<!-- NAME -->

## Name

My Digital Solar System

<!-- DESCRIPTION -->

## Description

This project is a web application that will allow you to discover all the system solar thanks to API : [Le Système Solaire](https://api.le-systeme-solaire.net/). This project has been implemented using the [VueJS](https://vuejs.org/) Framework.

<!-- CONTEXT -->

## Context

This project is a school project that will allow me to evaluate my skills concerning Javascript, and especially the [VueJS](https://vuejs.org/) Framework. The expected outcome was as follows: 

*List of stars*

A page listing all the stars should be available with the following features:
* Filter on criteria such as whether it is a planet (isPlanet) or not
* Filter on having moons

*Star page*

A page displaying a star : :
* All the information related to the on-call service must be displayed on this page.
* If the star is a planet, with moons, quick access to its moons must be available.

*Favorites page*

It should be possible to specify stars as favorites and remove them from the favorites list. When you add a star as a favorite, a notification should appear to let you know.
The display of stars on this page reuses the list display of the 'list of stars' page.

<!-- INSTALLATION -->

## Installation

To begin, you need to install the project's dependencies using the following command:

    yarn install

To do this, you need to install [yarn](https://yarnpkg.com/) (or you can use [npm](https://www.npmjs.com/))

<!-- USAGE -->

## Usage

You can start the application using the following command:

    yarn serve

Or is npm equivalent.

<!-- LICENSE -->

## License

[License](https://gitlab.com/Emilien.Gts/my-digital-stars/-/blob/master/LICENSE)

<!-- PROJECT STATUS -->

## Project Status

The project was awaiting rating, but the situation has changed. The project could no longer be done in VueJS, but in NextJS or NuxtJS. This project will therefore not be graded.
