import "bootstrap";
import "../node_modules/bootswatch/dist/minty/bootstrap.min.css";
import "jquery";
import "popper.js";

import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import Toaster from '@meforma/vue-toaster';

const app = createApp(App);
app.use(store);
app.use(router);
app.use(Toaster)
app.mount('#app')