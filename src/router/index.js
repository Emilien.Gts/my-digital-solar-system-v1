import { createWebHistory, createRouter } from "vue-router";
import HomePage from './../pages/HomePage.vue'
import PlanetsPage from './../pages/PlanetsPage.vue'
import PlanetPage from './../pages/PlanetPage.vue'
import FavoritesPage from './../pages/FavoritesPage.vue'

const routes = [
  {
    path: "/",
    name: "Home",
    component: HomePage,
  },
  {
    path: "/:id",
    name: "Planet",
    component: PlanetPage,
    props: true
  },
  {
    path: "/planets",
    name: "Planets",
    component: PlanetsPage,
  },
  {
    path: "/favorites",
    name: "Favorites",
    component: FavoritesPage,
  }
];

const router = createRouter({
  history: createWebHistory(),
  routes,
});

export default router;