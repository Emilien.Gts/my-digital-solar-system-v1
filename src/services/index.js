import axios from 'axios'

function findAll() {
  try {
    return axios
      .get("https://api.le-systeme-solaire.net/rest/bodies")
      .then(response => response.data.bodies)
  } catch (error) {
    console.log(error)
  }
}

function find(id) {
  try {
    return axios
      .get("https://api.le-systeme-solaire.net/rest/bodies/" + id)
      .then(response => response.data)
  } catch (error) {
    console.log(error)
  }
}

function findByUrl(url) {
  try {
    return axios
      .get(url)
      .then(response => response.data)
  } catch (error) {
    console.log(error)
  }
}

export default {
  findAll,
  find,
  findByUrl
}