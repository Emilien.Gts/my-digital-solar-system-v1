import { createStore } from 'vuex'

const state = {
  favorites: ['lune', 'io', 'terre', 'uranus']
}

const mutations = {
  addFavorite(state, favorite) {
    state.favorites.push(favorite)
  },
  deleteFavorite(state, favorite) {
    let i = '';
    state.favorites.map((f, index) => {
      f === favorite && (i = index);
    });

    i !== '' && state.favorites.splice(i, 1);
  }
}

const getters = {
  favorites: state => state.favorites
}

export default createStore({
  state,
  mutations,
  getters
})